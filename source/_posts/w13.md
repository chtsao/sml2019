---
title: Week 13. Ensemble Learning
date: 2019-05-13 16:10:05
tags: [Ensemble learning, boosting, random forest, weighted regression/classification]
---
![](http://clipart-library.com/img/1777597.png)
### Ensemble Learning

- Ensemble learing: [course slide](http://faculty.ndhu.edu.tw/~chtsao/ftp/sml/ensemblelearning.pdf) 
- C. A. Tsao (2014). [A statistical introduction to ensemble learning](http://faculty.ndhu.edu.tw/~chtsao/ftp/sml/statintr2ensemblelearning.pdf), 52, 115--132, JCSA.